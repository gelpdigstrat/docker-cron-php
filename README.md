# docker-php-cron

Automatically runs mysql dump against a set of databases daily using cron. Additional cron jobs can be added by placing them in `/etc/cron.d/`

## Configuration options

Use the following environment variables

* **DB_USER** - set to database username
* **DB_PASSWORD** - set to database password
* **DB_DATABASES** - path to file containing databases to backup
* **DB_BACKUP_PATH** - path to backup databases to


## Database Backup

In order to backup databases a json file with containing the databases to backup must be provided.

The format is as follows:

```js
[
  {
    "host": "host1",
    "databases": ["db1","db2"]
  },
  {
    "host":"host2",
    "databases": ["db3","db4"]
  }
]
```
