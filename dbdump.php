<?php

$db_user = @$_SERVER["DB_USER"] or die('DB_USER not specified');
$db_password = @$_SERVER["DB_PASSWORD"] or die('DB_PASSWORD not specified');
$db_databases_file = @$_SERVER["DB_DATABASES"] or die('DB_DATABASES not specified');
$backup_path = @$_SERVER["DB_BACKUP_PATH"] or die('DB_BACKUP_PATH not specified');


$time = time();
$day = date('j', $time);
if ($day == 1) {
        $date = date('Y-m-d', $time);
} else {
        $date = $day;
}

//import databases
file_exists($db_databases_file) or die('Databases file does not exist');
$databases = json_decode(file_get_contents($db_databases_file),true);

foreach( $databases as $host ) {
  $host_name = $host['host'];

  //create host directory if needed
  file_exists("$backup_path/$host_name") or mkdir("$backup_path/$host_name",0777);

  foreach($host['databases'] as $db) {
    $backupFile = "$backup_path/$host_name/${db}_${date}.sql.gz";
    if (file_exists($backupFile)) {
            unlink($backupFile);
    }

    $command = "mysqldump --opt -h $host_name -u $db_user -p'$db_password' $db  | gzip > $backupFile";

    $timestamp = date('c');
    print("[$timestamp] Backing up '$db' on '$host_name'\n");
    system($command);
  }
}

?>
