FROM php:7.1-cli

# Install cron
RUN set -ex; \
   \
   apt-get update; \
   apt-get install -y \
      cron \
      mysql-client \
      curl \
   ; \
   rm -rf /var/lib/apt/lists/*

# Add crontab file in the cron directory
ADD crontab /etc/crontab

# Give execution rights on the cron job
RUN chmod 0644 /etc/crontab

# Create the log file to be able to run tail
RUN touch /var/log/cron.log

# add db dump script
ADD dbdump.php /usr/src
RUN chmod 0644 /usr/src/dbdump.php

#Entrypoint Script
COPY docker-entrypoint.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/docker-entrypoint.sh

ENTRYPOINT ["docker-entrypoint.sh"]

# Run the command on container startup
CMD cron && tail -f /var/log/cron.log
