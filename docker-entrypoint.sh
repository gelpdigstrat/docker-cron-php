#!/bin/bash
set -euo pipefail

#make environment variables available to cron
env > /etc/environment

exec "$@"
